package com.Test.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        val fragmentatas = Atas()
        val fragmentbawah = Bawah()
        fragmentTransaction.add(R.id.FrameAtas,fragmentatas,fragmentatas.javaClass.simpleName)
        fragmentTransaction.add(R.id.FrameBawah,fragmentbawah,fragmentbawah.javaClass.simpleName)
        fragmentTransaction.commit()
    }
}
